"""
Exercise 1.
Зформуйте строку, яка містить певну інформацію про символ по його індексу в відомому слові.
Наприклад "The [індекс символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
Слово та номер отримайте за допомогою input() або скористайтеся константою.
Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".
"""
famous_word = str(input())
index = int(input())
print(f'The {index} symbol in {famous_word} is {famous_word[index]}')

"""
Exercise 2.
Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). 
Напишіть код, який визначить кількість слів, в цих даних.
"""

dictionary_string = str(input())
dictionary_list_string = dictionary_string.split()
result = {len(dictionary_list_string)}
print(f'Number of words in a line: {result}')

"""
Exercise 3.
Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.
"""

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []

for i in lst1:
    if type(i) in [int, float]:
        lst2.append(i)
result = lst2
print(F'А letter composed of int and float: {result}')
