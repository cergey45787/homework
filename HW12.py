"""
Завдання 1. Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу int або float.
"""


class Point:
    _x = 0
    _y = 0

    def __init__(self, x, y):
        """
         Initializer, creates a class variable
        :param x: (int or float)
        :param y: (int or float)
        """
        self.x = x
        self.y = y

    @property
    def x(self):
        """
        Class variable
        :return: int or float
        """
        return self._x

    @x.setter
    def x(self, value):
        """
        Variable check
        :param value: int or float
        """
        if isinstance(value, (int, float)):
            self._x = value
        else:
            raise TypeError('x = int or float')

    @property
    def y(self):
        """
        Class variable
        :return: int or float
        """
        return self._y

    @y.setter
    def y(self, value):
        """
        Variable check
        :param value: int or float
        """
        if isinstance(value, (int, float)):
            self._y = value
        else:
            raise TypeError('y = int or float')


"""
Завдання 2. Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
Реалізуйте перевірку даних, аналогічно до класу Line.
Визначет атрибут, що містить площу трикутника (за допомогою property).
Для обчислень можна використати формулу Герона.
"""


class Triangle:
    _p1 = None
    _p2 = None
    _p3 = None


    def __init__(self, p1, p2, p3):
        """
        Initializer, creates a class variable
        :param p1: Object of class Point
        :param p2: Object of class Point
        :param p3: Object of class Point
        """
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    @property
    def p1(self):
        """
        Reading a class variable
        :return: Object
        """
        return self._p1

    @p1.setter
    def p1(self, value):
        """
         Entry with class variable check
        :param value: Object class Point
        """
        if isinstance(value, Point):
            self._p1 = value
        else:
            raise TypeError('p1 = Point')


    @property
    def p2(self):
        """
        Reading a class variable
        :return: Object
        """
        return self._p2

    @p2.setter
    def p2(self, value):
        """
        Entry with class variable check
        :param value: Object class Point
        """
        if isinstance(value, Point):
            self._p2 = value
        else:
            raise TypeError('p2 = Point')


    @property
    def p3(self):
        """
        Reading a class variable
        :return: Object
        """
        return self._p3

    @p3.setter
    def p3(self, value):
        """
        Entry with class variable check
        :param value: Object class Point
        """
        if isinstance(value, Point):
            self._p3 = value
        else:
            raise TypeError('p3 = Point')


    @property
    def length_sides(self):
        """
        Calculates the length of a side of a triangle
        :return: (tuple)
        """
        length_1 = ((self.p1.x - self.p2.x) ** 2 + (self.p1.y - self.p2.y) ** 2) ** 0.5
        length_2 = ((self.p2.x - self.p3.x) ** 2 + (self.p2.y - self.p3.y) ** 2) ** 0.5
        length_3 = ((self.p3.x - self.p1.x) ** 2 + (self.p3.y - self.p1.y) ** 2) ** 0.5
        return length_1, length_2, length_3


    @property
    def area(self):
        """
        Calculates the area of a triangle using Heron's formula
        :return: (float) - area of a triangle
        """
        a = self.length_sides[0]
        b = self.length_sides[1]
        c = self.length_sides[2]
        p = (a + b + c) / 2
        area = (p * (p - a) * (p - b) * (p - c)) ** 0.5
        return area


p1 = Point(6, 7)
p2 = Point(5, 1)
p3 = Point(6, 5)

triangle_1 = Triangle(p1, p2, p3)

print(triangle_1.area)



