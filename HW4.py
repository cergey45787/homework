"""
1. Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів, які містять дві голосні літери підряд.
"""
vowel_letters = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y']
arbitrary_term = str(input())
list_arbitrary_term = arbitrary_term.split()
print(list_arbitrary_term)

words_pair_vowels = set()

for word in list_arbitrary_term:
    list_of_letters_in_word = []
    list_of_letters_in_word.extend(word)

    for letter in range(len(list_of_letters_in_word) - 1):
        if list_of_letters_in_word[letter] in vowel_letters and list_of_letters_in_word[letter + 1] in vowel_letters:
            words_pair_vowels.add(word)
if len(words_pair_vowels) == 0:
    print('There are no words with paired vowels')
else:
    print(f'The are {len(words_pair_vowels)} words with pair vowels: {words_pair_vowels}')

"""
2. Є два довільних числа які відповідають за мінімальну і максимальну ціну.
Є Dict з назвами магазинів і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
"buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. 
"""
prices_in_stores = {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324,\
                    "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}
list_of_stores = list(prices_in_stores.keys())
list_of_prices = list(prices_in_stores.values())
while True:
    try:
        min_price = float(input('Min price:'))
        max_price = float(input('Max price:'))
        if min_price < 0 or max_price < 0 or max_price < min_price:
            print('Invalid change range entered, please try again.')
            continue
        break
    except ValueError as e:
        print('Wrong format, please - enter prices in numbers:')

stores_in_range = []
for price in range(len(list_of_prices)):
    if min_price <= list_of_prices[price] <= max_price:
        stores_in_range.append(list_of_stores[price])

if len(stores_in_range) == 0:
    print('There are no stores in the price range')
else:
    print(f'The are {len(stores_in_range)} stores with price in range: {stores_in_range}')
