# """
# Задання1. Напишіть гру "Вгадай число":
# а. програма загадує число від 1 до 100 і користувач намагається його вгадати.
#  І. якщо вгадав - вивести повідомлення "Поздоровляємо!"
#  II. якщо невгадав і різниця між числом користувача і загаданим більше 10 - вивести повідомлення:
#     "Холодно", якщо 5-10 - "Тепло", якщо 1-4 "Гаряче".
# b.після того, як користувач вгадав число - запитати, чи хоче він повторити гру (Y\N)
# c.всі функцій, крім основної, помістіть в окремий файл і імпортуйте в основний файл.
# Завдання2. Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах
# і задекоруйте ним основну функцію гри.
# Після закінчення гри декоратор має сповістити, скільки тривала гра.
# """


import random
import time


def decorator_time(func):
    """
    Декоратор призначений для визначення часу затраченого на гру.
    """

    def wrapper():
        start_time = time.time()
        func()
        end_time = time.time()
        print('час гри: ', end_time - start_time, 'секунд')

    return wrapper


@decorator_time
def game():
    """
    Функція являється грою, що загадує рандомне число від 1 до 100.
    Користувач повинен вгадати це число, функція буде надавати підсказки, в близькому діапазоні.
    """
    random_number = random.randint(1, 100)
    while True:
        user_number = int(input('Ввудіть число: '))
        if user_number == random_number:
            print('Поздоровляємо!')
            break
        elif abs(user_number - random_number) > 10:
                print('Холодно')
        elif 5 <= abs(user_number - random_number) <= 10:
                print('Тепло')
        elif 1 <= (user_number - random_number) <= 4:
                print('Гаряче')

    user_answer = input('Хочеш повторити гру? (Y/N): ')
    if user_answer == 'Y':
        game()
    elif user_answer == 'N':
        print('Дякуємо за гру!')


game()


# def random_number(arg1=1, arg2=100):
#    """
#    Функція видає рандомне число в виданному діапазоні.
#    """
#    number = random.randint(arg1, arg2)
#
#
# def user_number():
#     user_number = int(input('Введіть число: '))
#
#     return  print(user_number)
#
# def treatment_result(user=user_number, random=random_number):
#     while True:
#         if user == random:
#             return print('Поздоровляємо!')
#         elif abs(user - random) > 10:
#             return print('Холодно')
#         elif 5 <= abs(user - random) <= 10:
#             return print('Тепло')
#         elif 1 <= (user - random) <= 4:
#             return print('Гаряче')
#
#
# def user_answer(input):
#     user = input('Хочеш повторити гру? (Y/N): ')
#     while True:
#         if user == 'Y':
#            return game()
#         elif user == 'N':
#            return print('Дякуємо за гру!')
#
# # @decorator_time
# def game():
#     random_number()
#     user_number()
#     treatment_result()
#     user_answer()
#
# game()