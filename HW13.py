"""
Завдання 1. Доопрацюйте класс Triangle з попередньої домашки наступним чином:
a. Oбʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
b. Перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3.
"""


class Point:
    _x = 0
    _y = 0

    def __init__(self, x, y):
        """
         Initializer, creates a class variable
        :param x: (int or float)
        :param y: (int or float)
        """
        self.x = x
        self.y = y

    @property
    def x(self):
        """
        Class variable
        :return: int or float
        """
        return self._x

    @x.setter
    def x(self, value):
        """
        Variable check
        :param value: int or float
        """
        if isinstance(value, (int, float)):
            self._x = value
        else:
            raise TypeError('x = int or float')

    @property
    def y(self):
        """
        Class variable
        :return: int or float
        """
        return self._y

    @y.setter
    def y(self, value):
        """
        Variable check
        :param value: int or float
        """
        if isinstance(value, (int, float)):
            self._y = value
        else:
            raise TypeError('y = int or float')


class Triangle:
    _point1 = None
    _point2 = None
    _point3 = None

    def __init__(self, point1, point2, point3):
        """
        Initializer, creates a class variable
        :param point1: Object of class Point
        :param point2: Object of class Point
        :param point3: Object of class Point
        """
        self.point1 = point1
        self.point2 = point2
        self.point3 = point3

    @property
    def point1(self):
        """
        Reading a class variable
        :return: Object
        """
        return self._point1

    @point1.setter
    def point1(self, value):
        """
         Entry with class variable check
        :param value: Object class Point
        """
        if isinstance(value, Point):
            self._point1 = value
        else:
            raise TypeError('point1 = Point')

    @property
    def point2(self):
        """
        Reading a class variable
        :return: Object
        """
        return self._point2

    @point2.setter
    def point2(self, value):
        """
        Entry with class variable check
        :param value: Object class Point
        """
        if isinstance(value, Point):
            self._point2 = value
        else:
            raise TypeError('point2 = Point')

    @property
    def point3(self):
        """
        Reading a class variable
        :return: Object
        """
        return self._point3

    @point3.setter
    def point3(self, value):
        """
        Entry with class variable check
        :param value: Object class Point
        """
        if isinstance(value, Point):
            self._point3 = value
        else:
            raise TypeError('point3 = Point')

    @property
    def length_sides(self):
        """
        Calculates the length of a side of a triangle
        :return: (tuple)
        """
        length_1 = ((self.point1.x - self.point2.x) ** 2 + (self.point1.y - self.point2.y) ** 2) ** 0.5
        length_2 = ((self.point2.x - self.point3.x) ** 2 + (self.point2.y - self.point3.y) ** 2) ** 0.5
        length_3 = ((self.point3.x - self.point1.x) ** 2 + (self.point3.y - self.point1.y) ** 2) ** 0.5
        return length_1, length_2, length_3

    @property
    def area(self):
        """
        Calculates the area of a triangle using Heron's formula
        :return: (float) - area of a triangle
        """
        a = self.length_sides[0]
        b = self.length_sides[1]
        c = self.length_sides[2]
        p = (a + b + c) / 2
        area = (p * (p - a) * (p - b) * (p - c)) ** 0.5
        return area

    def __eq__(self, other):
        return self.area == other.area

    def __ne__(self, other):
        return self.area != other.area

    def __gt__(self, other):
        return self.area > other.area

    def __ge__(self, other):
        return self.area >= other.area

    def __lt__(self, other):
        return self.area < other.area

    def __le__(self, other):
        return self.area <= other.area

    def __str__(self):
        return f'{self.point1.x}, {self.point1.y} -- {self.point2.x}, {self.point2.y} -- {self.point3.x}, {self.point3.y}'


point_1 = Point(6, 7)
point_2 = Point(5, 1)
point_3 = Point(6, 5)
point_4 = Point(10, 3)
point_5 = Point(2, 6)
point_6 = Point(1, 9)

triangle_1 = Triangle(point_1, point_2, point_3)

triangle_2 = Triangle(point_4, point_5, point_6)

print(str(triangle_1))
print(str(triangle_2))

print(triangle_1 == triangle_2)
print(triangle_1 != triangle_2)
print(triangle_1 > triangle_2)
print(triangle_1 >= triangle_2)
print(triangle_1 < triangle_2)
print(triangle_1 <= triangle_2)