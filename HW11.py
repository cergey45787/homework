"""
Завдання 1. Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".
"""

class TransportSowing:
    color = None
    maximum_speed = None
    maximum_passengers = None
    price = None

    def __init__(self, color, maximum_speed, maximum_passengers, price):
        """
        Initializer, creates a class variable.
        :param color: (str) - color Transport Sowing.
        :param maximum_speed: (str) - maximum speed Transport Sowing.
        :param maximum_passengers: (str) - maximum passengers Transport Sowing.
        :param price: (str) - price Transport Sowing.
        """
        self.color = color
        self.maximum_speed = maximum_speed
        self.maximum_passengers = maximum_passengers
        self.price = price





class Car(TransportSowing):
    brand_car = None
    type_car_body = None

    def __init__(self, color, maximum_speed, maximum_passengers, price, brand_car, type_car_body):
        """
              Initializer, creates a class variable.
              :param brand_car: (str) - brand car.
              :param type_car_body: (str) - type_car_body.
         """
        self.color = color
        self.maximum_speed = maximum_speed
        self.maximum_passengers = maximum_passengers
        self.price = price
        self.brand_car = brand_car
        self.type_car_body = type_car_body


class Plane(TransportSowing):
    function_plane = None
    flight_range = None

    def __init__(self, color, maximum_speed, maximum_passengers, price, function_plane, flight_range):
        """
        Initializer, creates a class variable.
        :param function_plane: (str) - shows the destination of the aircraft.
        :param flight_range: (str) - shows the flight range of the aircraft.
        """
        self.color = color
        self.maximum_speed = maximum_speed
        self.maximum_passengers = maximum_passengers
        self.price = price
        self.function_plane = function_plane
        self.flight_range = flight_range


class Ship(TransportSowing):
    types_ship = None
    classification_ships = None

    def __init__(self, color, maximum_speed, maximum_passengers, price, types_ship, classification_ships):
        """
        Initializer, creates a class variable.
        :param types_ship: (str) - takes the term indicating the type of ship.
        :param classification_ships: (str) - takes the term indicating the class of the ship.
        """
        self.color = color
        self.maximum_speed = maximum_speed
        self.maximum_passengers = maximum_passengers
        self.price = price
        self. types_ship = types_ship
        self.classification_ships = classification_ships


transport_sowing = TransportSowing(color='Black', maximum_speed='15', maximum_passengers='2', price='500')

car = Car(color='Red', maximum_speed='200', maximum_passengers='4', price='90000', brand_car='Mercedes-Benz', type_car_body='Sedan')

plane = Plane(color='white', maximum_speed='890', maximum_passengers='818', price='24000000', function_plane='Passengers', flight_range='9800')

ship = Ship(color='gray', maximum_speed='39', maximum_passengers='2800', price='105000000', types_ship='warship', classification_ships='linkor')
