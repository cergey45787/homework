import library

assert library.is_string_capitalized('My name is David') is False
assert library.is_string_capitalized('I love playing') is True
assert library.is_string_capitalized('') is True
assert library.is_string_capitalized('565656') is True


assert library.is_even_number(1) is False
assert library.is_even_number(8) is True
assert library.is_even_number(0) is True
assert library.is_even_number(-3) is False
