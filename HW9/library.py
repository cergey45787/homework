"""
Завдання 1.
Функцію is_string_capitalized потрібно дописати
(в тому числі докстрінги) - очікування - прохід тестів.
"""


def is_string_capitalized(string):
    """
    Function check if the given string is already capitalised
    Args: any string

    Returns: True|False
    """
    string = string.strip()
    if string == string.capitalize():
        return True
    return False


"""
Завдання 2.
Напишіть функцію, яка перевірить, чи дане число є парним чи непарним (коментар з коду видаліть) ТА ПОКРИЙТЕ ЇЇ ТЕСТАМИ
"""


def is_even_number(number):
    """
    This function checks if the given number is odd or even.
    :param number: int
    :return: bool True|False
    """
    if number % 2 == 0:
        return True
    else:
        return False

